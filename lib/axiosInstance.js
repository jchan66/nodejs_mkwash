import Axios from 'axios'
import { retryAdapterEnhancer } from 'axios-extensions'
import UserAgent from 'user-agents'

const axiosInstance = Axios.create({
  withCredentials: true,
  baseURL: `http://${process.env.VERCEL_URL}`,
  headers: {
    'user-agent': new UserAgent().toString(),
  },
})

axiosInstance.interceptors.request.use(config => {
  config.headers.timestamp = Date.now()
  config.adapter = retryAdapterEnhancer(axiosInstance.defaults.adapter, { times: 3 })
  return config
})

axiosInstance.interceptors.response.use(response => {
  const now = Date.now()
  console.log([
    response.config.method.toUpperCase(),
    // (response.config.baseURL || '') + response.config.url,
    new URL(response.config.url, response.config.baseURL).toString(),
    `(${response.status})`,
    `[${now - response.config.headers.timestamp}ms]`,
    '|',
    now,
  ].join(' '))
  return response
}, error => {
  const now = Date.now()
  console.log([
    error.config.method.toUpperCase(),
    // (error.config.baseURL || '') + error.config.url,
    new URL(error.config.url, error.config.baseURL).toString(),
    `(${error.response.status})`,
    `[${now - error.config.headers.timestamp}ms]`,
    '|',
    now,
  ].join(' '))
  console.error(error.name)
  console.error(error.message)
  console.error(error.code)
  console.error(error.stack)
  // console.warn(error.name, error.message, error.)
  return null
})

export default axiosInstance