import axiosInstance from '../../lib/axiosInstance'
import ping from '../../lib/ping'

export default async (req, res) => {
  await Promise.race([].concat(
    axiosInstance.get('/api/hello-world'),
    axiosInstance.get('/api/job2'),
    axiosInstance.get('/api/job3'),
    0,
  ))
  await ping('/api/cron/ping', req, res)
}