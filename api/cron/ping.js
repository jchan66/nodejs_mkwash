import ping from '../../lib/ping'

export default async (req, res) => {
  await ping('/api/cron', req, res)
}